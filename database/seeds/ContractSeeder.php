<?php

use App\Contract;
use Illuminate\Database\Seeder;

class ContractSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contract::create([
            'name' => 'Clasico',
            'price' => 1000,
            'is_active' => true
        ]);

        Contract::create([
            'name' => 'Vip',
            'price' => 2000,
            'is_active' => true
        ]);

        Contract::create([
            'name' => 'Premium',
            'price' => 3000,
            'is_active' => true
        ]);
    }
}
