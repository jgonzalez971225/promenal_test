<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_order');
            $table->text('address_order')->nullable();
            $table->string('order_id')->unique();
            $table->integer('auth_code');
            $table->string('last4_card');
            $table->string('payment_status');
            $table->string("monthly_installments");
            $table->string("name_card");
            $table->string("exp_month_card");
            $table->string('exp_year_card');
            $table->unsignedBigInteger('contract_id');
            $table->foreign('contract_id')->references('id')->on('contracts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
