@extends('layout')

@section('content')

    <div class="col-md-4 mt-5">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <span>Payment Detail</span>
                <span class="badge badge-secondary">
                    {{$payment->order_id}}
                    # {{$payment->id}}
                </span>
            </div>
            <div class="card-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <strong>Customer:</strong> {{$payment->name_order}}
                    </li>
                    <li class="list-group-item">
                        <strong>Amount:</strong> ${{number_format($payment->contract->price)}} MXN
                    </li>
                    <li class="list-group-item">
                        <strong>Address:</strong> {{$payment->address_order}}
                    </li>
                    <li class="list-group-item">
                        <strong>Monthly Installments:</strong> {{$payment->monthly_installments}}
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-8 mt-5">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <span>Detail Card</span>
                <span class="badge badge-secondary badge-bill text-uppercase">
                    {{$payment->payment_status}}
                </span>
            </div>
            <div class="card-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <strong>Contract: </strong> {{$payment->contract->name}} (${{number_format($payment->contract->price)}} MXN)
                    </li>
                    <li class="list-group-item">
                        <strong>No. Auth: </strong> {{$payment->auth_code}}
                    </li>
                    <li class="list-group-item">
                        <strong>Card Name: </strong> {{$payment->name_card}} 
                        **** {{ $payment->last4_card }}
                    </li>
                    <li class="list-group-item">
                        <strong>Exp Month Card: </strong> {{$payment->exp_month_card}} 
                    </li>
                    <li class="list-group-item">
                        <strong>Exp Year Card: </strong> {{$payment->exp_year_card}} 
                    </li>
                </ul>
            </div>
        </div>
    </div>

@endsection