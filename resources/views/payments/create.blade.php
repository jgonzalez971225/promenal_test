@extends('layout')

@section('content')
    <form action="{{asset('payments')}}" method="POST" class="col-md-12" id="card-form">
        @csrf
        @include('notifications.notifications')
        @include('payments.form')
    </form>
@endsection

@section('js')
   <script src="{{asset('js/conektatokenid.js')}}"></script>
@endsection