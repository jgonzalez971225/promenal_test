<div class="row">
    <div class="col-md-7 mt-5 ma-auto">
        <div class="card">
            <div class="card-header text-uppercase d-flex justify-content-center align-items-start">
                <span>Personal Informatión</span>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Full Name</label>
                            <input type="text"  required value="{{old('name')}}" name="name" data-conekta="card[name]" class="form-control">
                            @include('payments.error_field',['field' => 'name'])
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="text"  required requiredvalue="{{old('email')}}" name="email" class="form-control">
                            @include('payments.error_field',['field' => 'email'])
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Phone</label>
                            <input type="text" required name="phone" value="{{old('phone')}}" class="form-control">
                            @include('payments.error_field',['field' => 'phone'])
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mt-3">
        <div class="card-header text-uppercase d-flex justify-content-center align-items-start">
                <span>Address</span>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6 mt-2">
                        <div class="form-group">
                            <label for="">Street 1   </label>
                            <input name="street1" required value="{{old('street1')}}" class="form-control" placeholder="" />
                            @include('payments.error_field',['field' => 'street1'])
                        </div>
                    </div>
                    <div class="col-md-6 mt-2">
                        <div class="form-group">
                            <label for="">Postal Code </label>
                            <input name="postal_code" required value="{{old('postal_code')}}" class="form-control" placeholder="" />
                            @include('payments.error_field',['field' => 'postal_code'])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5 mt-5 ma-auto">
        <div class="card">
            <div class="card-header text-uppercase d-flex justify-content-center align-items-start">
                <span>Payment</span>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Select a contract</label>
                            <select required name="contract_id" class="form-control" id="">
                                <option value="" selected>Select one option</option>
                                @foreach($contracts as $contract)
                                    <option value="{{$contract->id}}">{{$contract->name}} (${{number_format($contract->price,2)}})</option>
                                @endforeach
                            </select>
                            @include('payments.error_field',['field' => 'contract_id'])
                        </div>
                    </div>
                    <div class="col-md-12">
                        <span class="card-errors text-danger"></span>
                        <div class="form-group">
                            <label for="">Numero de tarjeta</label>
                            <input class="form-control" required size="20" data-conekta="card[number]" type="text">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">CVV</label>
                            <input class="form-control" required size="4" data-conekta="card[cvc]" type="text">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <span>Fecha Exp. (MM/AAAA)</span>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <input class="form-control"  requiredsize="4" data-conekta="card[exp_month]" type="text">
                                </div>
                            </div>
                            
                            <div class="col-md-5">
                                <div class="form-group">
                                    <input class="form-control" required  size="4" data-conekta="card[exp_year]" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="">Meses sin intereses</label> <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="monthly_installments" checked id="monthly_installments_3" value="3">
                                <label class="form-check-label" for="monthly_installments_3">3</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="monthly_installments" id="monthly_installments_6" value="6">
                                <label class="form-check-label" for="monthly_installments_6">6</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="monthly_installments" id="monthly_installments_9" value="9">
                                <label class="form-check-label" for="monthly_installments_9">9</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="monthly_installments" id="monthly_installments_12" value="12">
                                <label class="form-check-label" for="monthly_installments_12">12</label>
                            </div>
                            @include('payments.error_field',['field' => 'monthly_installments'])
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-success">Payment</button>
            </div>
        </div>
    </div>
</div>