@extends('layout')

@section('content')
    @include('notifications.notifications')
    <div class="col-md-12 mt-5">
        <div class="d-flex justify-content-between align-items-center mb-3" >
            <h4>Payments</h4>
            <a href="{{asset('payments/create')}}" class="btn btn-default">
                Create Payment
            </a>
        </div>
        <table class="table table-striped table-hover">
            <thead class="thead-dark ">
                <tr>
                    <th class="text-center">Contract</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Address</th>
                    <th class="text-center">Card Type</th>
                    <th class="text-center">No. Auth</th>
                    <th class="text-center">Detail</th>
                </tr>
            </thead>
            <tbody>
                @foreach($payments as $payment)
                    <tr class="text-center">
                        <td>
                            {{$payment->contract->name}}
                        </td>
                        <td>
                            <span class="badge badge-secondary badge-pill ">
                                {{$payment->payment_status}}
                            </span>
                        </td>
                        <td>
                            {{$payment->name_order}}
                        </td>
                        <td>
                            {{$payment->address_order}}
                        </td>
                        <td>
                            <span class="badge badge-secondary text-uppercase badge-pill">
                                {{$payment->name_card}} **** {{ $payment->last4_card }}
                            </span>
                        </td>
                        <td>
                            {{$payment->auth_code}}
                        </td>
                        <td>
                            <a href="{{asset('payments/'.$payment->order_id)}}">Detail</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{$payments->links()}}
    </div>
@endsection