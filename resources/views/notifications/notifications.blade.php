@if(Session::has('status_success'))
    <div class="col-md-12 mt-5 mb-2">
        <div class="alert alert-success">
            <strong> {{Session::get('status_success')}} </strong>
        </div>
    </div>
@endif
@if(Session::has('status_warning'))
    <div class="col-md-12 mt-5 mb-2">
        <div class="alert alert-warning">
            <strong> {{Session::get('status_warning')}} </strong>
        </div>
    </div>
@endif