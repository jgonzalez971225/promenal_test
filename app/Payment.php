<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    
    protected $guarded = [];

    public function contract() 
    {
        return $this->belongsTo(Contract::class);
    }

    public function scopeItems($query) 
    {
        return $query->with(['contract:id,name,price']);
    }

    public function scopeFirstWhereOrderId($query,$orderId)
    {
        return $query->where('order_id',$orderId)->with(['contract:id,name,price']);
    }

}
