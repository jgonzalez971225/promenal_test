<?php

namespace App\Http\Controllers;

use App\Contract;
use App\Http\Requests\PaymentRequest;
use App\Payment;
use App\Resolvers\PaymentPlatformResolver;

class PaymentController extends Controller
{
    
    protected $paymentPlatformResolver;

    public function __construct(PaymentPlatformResolver $paymentPlatformResolver) 
    {
        $this->paymentPlatformResolver = $paymentPlatformResolver;
    }

    public function index()
    {
        return view('payments.index',[
            'payments' => Payment::items()->latest()->paginate(5)
        ]);
    }   

    public function create()
    {
        return view('payments.create',[ 'contracts' => Contract::items()->get() ]);
    }

    public function store(PaymentRequest $request)
    {
        try{

            $paymentPlatform = $this->paymentPlatformResolver->resolveService('conekta');

            $payment = $paymentPlatform->handlePayment($request);

            if (!$payment['status']) {
                return back()->with('status_warning',$payment['message']);
            }

            Payment::create($payment['payment']);

            return redirect('/')->with('status_success','Payment Created');

        }catch(\Exception $e ){
            return redirect('/')->with('status_success',$e->getMessage());
        }

    }

    public function show($orderId)
    {
        $payment = Payment::firstWhereOrderId($orderId)->firstOrFail();
        return view('payments.show',['payment' => $payment]);
    }

}
