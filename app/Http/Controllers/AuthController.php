<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    
    public function form() 
    {
        return view('login');
    }

    public function auth(Request $request) 
    {

        $user = User::whereEmailAddress($request->email)->first();

        if (!$user){ 
            return back();
        }

        if ( ! Hash::check( $request->password , $user->password) ) {
            return back();
        }

        $authUser = Auth::loginUsingId($user->id);

        return redirect('/');

    }

    public function logout() 
    {
        Auth::logout();
        return redirect('login');
    }

}
