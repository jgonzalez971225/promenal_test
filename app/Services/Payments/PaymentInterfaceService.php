<?php namespace App\Services\Payments;

interface PaymentInterfaceService 
{

    public function getProduct($productId);

    public function handlePayment($request);

    public function buildPayloadPayment(object $order , $productId);

}