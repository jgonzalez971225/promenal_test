<?php namespace App\Services\Payments;
use Conekta\Conekta;
use Conekta\Order as ConektaOrder;
use App\Services\Payments\PaymentInterfaceService;
use App\Services\Payments\Traits\PaymentOrderResolver;
use App\Services\Payments\Traits\ProductResolver;

class ConektaService implements PaymentInterfaceService
{

    use ProductResolver , PaymentOrderResolver;

    public function __construct()
    {
        Conekta::setApiKey(env('CONEKTA_PRK'));
        Conekta::setApiVersion(env('CONEKTA_VERSION'));
       
    }

    public function handlePayment($request)
    {

        $product                   = $this->getProduct($request->contract_id);

        $lineItems[]               = $this->buildLineItems($product);

        $order['line_items']       = $lineItems;
        $order['currency']         = $request->currency ?? 'MXN';
        $order['customer_info']    = $this->buildCustomerInfo();
        $order['shipping_contact'] = $this->buildShippingContact();
        $order['charges']          = $this->buildCharges(); 

        try {
            
            $conektaOrder = ConektaOrder::create($order);
            return $this->buildPayloadPayment($conektaOrder,$product->id);

        } catch (\Conekta\ParameterValidationError $ex) {

            $error['status']  = false;
            $error['code']    = $ex->getCode();
            $error['message'] = $ex->getMessage();
            return $error;

        } catch(\Conekta\Handler $handleError) {
            
            $error['status']  = false;
            $error['code']    = $handleError->getCode();
            $error['message'] = $handleError->getMessage();
            return $error;

        }
    }

    protected function getPriceInCents($centsPrice)
    {
        return $centsPrice * 100;
    }

    protected function buildLineItems($product) 
    {
        return [
            "name"       => $product->name,
            "unit_price" => $this->getPriceInCents($product->price),
            "description" => "unique",
            "quantity"    => 1
        ];
    }

    protected function buildCustomerInfo()
    {
        return [
            "name" => request()->name,
            "email" => request()->email,
            "phone" => request()->phone
        ];
    }

    protected function buildShippingContact() 
    {
        return [
            'address' => [
                "street1" => request()->street1,
                "postal_code" => request()->postal_code,
                "country" => "MX"
            ]
        ];
    }

    protected function buildCharges() 
    {
        return [
            [
                "payment_method" => array(
                    "type" => "card",
                    "token_id" => request()->conektaTokenId,
                    "monthly_installments" => request()->monthly_installments,
                )
            ]
        ];
    }
}