<?php namespace App\Services\Payments\Traits;

use App\Contract;

trait ProductResolver 
{

    public function getProduct($productId)
    {
        return Contract::findOrFail($productId, ['id','name','price']);
    }

}