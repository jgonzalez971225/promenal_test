<?php namespace App\Services\Payments\Traits;

trait PaymentOrderResolver 
{


    public function buildPayloadPayment($order , $productId)
    {
        $address       = $order->shipping_contact->address;
        $charges       = $order->charges[0];
        $paymentMethod = $charges->payment_method;

        return [
            'status' => true,
            'payment' => [
                'name_order'           => $order->customer_info['name'],
                'address_order'        => $address->street1.' '.$address->postal_code.' '.$address->country,
                'order_id'             => $order->id,
                'auth_code'            => $paymentMethod->auth_code,
                'last4_card'           => $paymentMethod->last4,
                'payment_status'       => $order->payment_status,
                'monthly_installments' => $charges->monthly_installments,
                'name_card'            => $paymentMethod->issuer,
                'exp_month_card'       => $paymentMethod->exp_month,
                'exp_year_card'        => $paymentMethod->exp_year,
                'contract_id'          => $productId
            ]
        ];
    }

}