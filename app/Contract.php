<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    
    protected $guarded = [];

    public function scopeItems($query) 
    {
        return $query->where('is_active',true)->orderBy('name','asc'); 
    }
}
