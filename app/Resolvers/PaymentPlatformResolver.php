<?php namespace App\Resolvers;
use Exception;
class PaymentPlatformResolver 
{

    
    public function resolveService($platform)  
    {
        $service = config("services.{$platform}.class");

        if ($service) {
            return resolve($service);
        }

        throw new Exception('The selected payment platform is not in the configuration');
    }

}