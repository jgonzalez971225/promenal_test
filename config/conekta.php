<?php

return [
    "test_mode" => [
        'cards' => [
            "4242424242424242"  => "tok_test_visa_4242",
            "4012888888881881"  => "tok_test_visa_1881",
            "5555555555554444"  => "tok_test_mastercard_4444",
            "5105105105105100"  => "tok_test_mastercard_5100",
            "378282246310005"   => "tok_test_amex_0005",
            "371449635398431"   => "tok_test_amex_8431",
            "4915669353237603"  => "tok_test_banorte_debit"
        ]
    ]
];