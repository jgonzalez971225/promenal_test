<?php

use App\Http\Controllers\PaymentController;
use Illuminate\Support\Facades\DB;


Route::get('login','AuthController@form')->middleware('guest');
Route::post('auth','AuthController@auth')->middleware('guest');

Route::get('_logout','AuthController@logout')->middleware('auth');

Route::get('/','PaymentController@index');
Route::get('payments/create','PaymentController@create')->middleware('auth');
Route::get('payments/{orderId}','PaymentController@show')->middleware('auth');
Route::post('payments','PaymentController@store')->middleware('auth');